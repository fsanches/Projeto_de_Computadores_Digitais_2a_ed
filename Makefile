PAGES := $(patsubst %.tiff,%.jpg,$(wildcard *.tiff))

PDF = Livro.pdf

all: $(PDF)

clean:
	rm -rf $(PAGES)
	rm $(PDF)

%.jpg: %.tiff
	convert $< -resize 800 $@

$(PDF): $(PAGES)
	convert $(PAGES) $(PDF)
	evince $(PDF)
