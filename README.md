Recebi autorização do professor Edson Fregni em Maio de 2016 para escanear e publicar na Internet, o livro "Projeto de Computadores Digitais".


O PDF final gerado a partir deste escaneamento da 2ª edição (publicada em 1979) também se encontra junto a outros documentos relativos ao projeto do Patinho Feio, o primeiro computador brasileiro (de 1971/72), no seguinte repositórogo GitHub:

https://github.com/felipesanches/PatinhoFeio
